
=======================================================================================
Lundi 21 septembre 2020 **Boker Tov de ce matin – Téchouva et Maïmonide**
=======================================================================================

.. seealso::

   - https://rabbinchinsky.fr/2020/09/21/boker-tov-de-ce-matin-techouva-et-maimonide/

.. contents::
   :depth: 3


.. _libre_arbitre_maimonide:

HilHot Téchouva 5:1 – Le libre arbitre
=========================================


Le libre arbitre est donné à chaque être humain.

S’il veut incliner sa personnalité vers le bien et devenir un juste, le
choix est entre ses mains ; et s’il veut incliner sa personnalité vers
le mal et devenir une personne mauvaise, le choix est entre ses mains.

Tel est le sens de ce qui est écrit dans la Torah : « ainsi, l’être humain
sera semblable à l’un de nous sachant le bien et le mal ».

Cela signifie que le genre humain est unique en cela, et qu’aucune
autre espèce ne lui ressemble en ce fait : Par lui-même, par sa connaissance
et par sa pensée, il sait le bien et le mal et réalise ce à quoi il aspire.
Personne ne l’empêche de faire le bien ou le mal, et puisqu’il en est ainsi,
qu’il prenne garde de ne pas mal agir


.. _vidoui_maimonide:

HilHot Téchouva 1:1 – Le Vidouï
===================================


Tous les commandements qui se trouvent dans la torah, qu’ils soient positifs
ou négatifs, si quelqu’un a enfreint l’un d’eux volontairement ou
involontairement, lorsqu’il fera téchouva et reviendra de sa faute il a
l’obligation de passer aux aveux devant hael, il est béni, comme il est
dit « Quand un homme ou une femme feront… et ils passeront aux aveux de
leur faute, c’est ce qu’on appelle l’aveu des fautes, c’est un commandement
positif, comment on avoue ? On dit « je t’en prie hachem, j’ai fauté,
j’ai fait le mal, j’ai commis des pêchés devant toi et j’ai fait telle
et telle chose et voici que je regrette et que j’ai honte de mes actes
et que je n’y reviendrai plus jamais…
