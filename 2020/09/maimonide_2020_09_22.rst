

.. _maimonide_2020_09_22:

=======================================================================================
Mardi 22 septembre 2020 Téchouva et Maïmonide – deux autres extraits pour ce matin
=======================================================================================

.. seealso::

   - https://rabbinchinsky.fr/2020/09/22/techouva-et-maimonide-deux-autres-extraits-pour-ce-matin/

.. contents::
   :depth: 3


Introduction
==============


Deux  nouveaux extraits du michné torah de Maïmonide pour le boker tov
de ce matin…

Ci-dessous, la playlist des Boker Tov/ SeliHot.

L’épisode d’hier est aujourd’hui le dernier de la playlist, celui
d’aujourd’hui sera visible dés ce soir.

Bonne journée à toutes et tous!


.. _maimonide_pardon_de_dieu:

Michné Torah / HilHot Téchouva 1 :3 – Pardon de Dieu
========================================================


Et en ces temps où la maison sainte n’existe plus et que nous n’avons
plus d’autel pour le pardon, il ne reste plus que la téchouva, la téchouva
fait expiation de toutes les fautes, mêmes pour celui qui a été mauvais
tous les jours de sa vie et a fait téchouva dernièrement, on ne le lui
rappelle rien de ses actions mauvaises comme il est dit : la méchanceté
du méchant ne lui fera pas obstacle au jour où il reviendra de sa méchanceté,
et l’essence de yom kippour est de recouvrir pour ceux qui reviennent
comme il est dit : car en ce jour il sera fait expiation.


.. _maimonide_pardon_du_prochain:

Michné Torah / HilHot Téchouva 1 :9 – Pardon du prochain
============================================================

La téchouva et le jour de kippour ne font expiation que pour les fautes
entre l’homme et Dieu … mais les fautes qui sont entre l’homme et son
prochain, comme celui qui vole à son prochain ou qui insulte son prochain
et autres, il n’est pas pardonné tant qu’il n’a pas donné à son prochain
ce qu’il lui doit et qu’il l’aie contenté, et quand bien même il lui
aurait restitué l’argent qu’il lui doit, il doit le contenter et lui
demander son pardon… il doit l’apaiser et essayer de le toucher jusqu’à
ce qu’il lui pardonne, et si son prochain ne veut pas lui pardonner, il
lui amène un groupe de trois de ses amis et ils essayent de le toucher
et ils lui demandent, s’il n’est pas satisfait de cette démarche, il
lui amène une deuxième fois et une troisième, s’il ne veut toujours pas,
il le laisse et s’en va et celui qui n’a pas pardonné, c’est lui le
fauteur, et s’il s’agit de son rabbin il va et vient même mille fois
jusqu’à ce qu’il lui pardonne.
