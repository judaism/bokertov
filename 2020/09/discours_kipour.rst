.. index::
 ! Kopour; 5781
 ! Kipour 5781


.. _discours_kipour_2020_09_27:

=================================================================================
**Discours kipour** Une seule solution: La transgression
=================================================================================

.. seealso::

   - https://rabbinchinsky.fr/2020/09/27/discours-kipour/



Il y a dix jours commençait notre voyage dans l’année 5781.

Dix jours sont passés. Je ne suis pas plus avancée. Cette année, je ne sais pas.

Lorsque j’imagine qu’à Roch hachana le monde aurait été jugé et qu’aujourd’hui
son sort serait scellé, lorsque je pense à cela et que j’observe ce que
je vois du monde, je ressens de la tristesse, je ressens de la colère.

Et je sais que j’ai besoin de RaHamim, de largesse, d’un endroit à l’intérieur
de moi pour accueillir ces sentiments.
Ce soir, je suis heureuse de me projeter dans ces 25 heures de recherche
commune, de laisser ma colère et ma tristesse être portées par nos textes
ancestraux et par notre engagement indéfectible.

Comme moi, vous écoutez l’actualité, vous êtes témoins des misères politiques
autant que de la misère de nos rues, vous vivez des douleurs du corps et
de l’émotion. Nous partageons cette condition humaine.

Nos ancêtres, à toutes les générations, l’ont également partagée.

Nous avons toujours considéré deux choses :

1 – nous voulons savoir
2 – nous refusons de subir

Quel que soit le prix du savoir et de l’action, nous avons toujours voulu
payer ce prix, le savoir et l’action valent toutes les peines, tous les
risques, quitte à transgresser en leur nom.

1 – Nous voulons savoir.
    La première femme et le premier homme ont mangé du fruit de l’arbre
    de la connaissance du bien et du mal, au risque de devoir quitter à
    jamais le paradis des illusions.

    Abraham et Sarah ont voulu savoir, refusé d’idolâtrie, posé les
    questions interdites, au risque d’être pourchassés par Nemrod, au
    prix de l’exil.

    Moïse a voulu savoir, il a quitté l’Égypte oppressive, vers des
    territoires inconnus, il s’est approché du buisson ardent pour
    comprendre.

    Ezra et Néhémie ont voulu savoir, ils ont eu le courage de fonder
    l’institution des scribes.

    Rabbi Akiva a voulu savoir, il a mis l’enseignement en priorité, au
    péril de sa vie, comme plus tard Rabbi YoHanan ben zakai, et tous
    nos sages à travers les siècles.

    Nous voulons savoir, même si le savoir peut faire mal, même s’il
    demande la transgression des lois humaines ou divines.

2 – Nous refusons de subir.
    La première femme et le premier homme ont refusé de se soumettre à
    l’interdit divin, Abraham et Sarah ont décidé de leur avenir et mis
    en place un enseignement, pris un rôle de leaders,

    Moïse a refusé le lavage de cerveau de l’intelligentsia égyptienne
    déshumanisant les hébreux pour justifier leur esclavage, il est passé
    à l’action en acceptant de devenir le leader d’un peuple d’esclaves
    mal éduqués,

    Ezra et Néhémie ont construit le deuxième Temple, Rabbi Akiva a
    soutenu des révoltes et Rabbi Juda le prince a rédigé un enseignement
    censé rester oral, et il en fut ainsi pour tous les sages et tous
    les activistes de notre peuple à travers les siècles.

    Nous refusons de subir.

    Nous voulons agir, même si l’action est dangereuse, même si elle
    transgresse les lois humaines ou divines.

Avec le recul de l’histoire, nous savons ce qu’eux-mêmes ignoraient, nous
savons ce qu’ils ont accompli.
Ce que nous ignorons, ce sont les doutes qu’ils ont traversés.

« Ce qu’il faut de malheur pour la moindre chanson Ce qu’il faut de
regrets pour payer un frisson Ce qu’il faut de sanglots pour un air de guitare »

Et lorsque les malheurs, les regrets, les sanglots, nous assaillent, nous
ne savons rien des chansons, des frissons, des savoirs, des actions que
ces souffrances peuvent nous acquérir.

Le Talmud nous raconte le désespoir d’Adam et Eve.
Combien de fois dans la Torah Abraham ne doute-t-il pas de sa destinée ?
Combien de fois Moïse désespère-t-il de l’accomplissement de sa mission ?
Rabbi Akiva avoue lui-même, à l’heure de sa mort, qu’il a douté jusqu’au
dernier moment être capable d’être fidèle à ses valeurs.

Eve ne savait pas que cinq mille ans plus tard, hommage lui serait rendu.
Abraham ignorait que l’on mentionnerait encore son alliance quatre mille
en plus tard.
Il y a trois mille ans, Moïse ne pouvait deviner que nous serions encore
les dépositaires du chant Haazinou que nous avons lus dans toutes les
synagogues chabbat dernier.
Ezra, il y a 2500 ans, n’imaginait pas ce que deviendrait son alliance,
pas plus que Rabbi Akiva, il y a 2000 ans.
Nous ne savons pas ce qui restera des actes et des savoirs que nous
aurons transmis.

Nous ne savons rien de ce que nous réserve l’avenir, nous ne savons rien
de ce que réserve l’avenir à l’humanité, ni au peuple juif.

Nous savons simplement que nous avons aujourd’hui, comme dans les siècles
passés, des sentiments face au présent, une soif de savoir et une soif
d’agir.
Toutes ces choses alimentent notre détermination. Au point d’être présents
dans ce monde,

- au meilleur de nos possibilités, au point de rêver d’un monde meilleur,
- au maximum de notre intelligence, au point de tout remettre en cause,
- au plus haut de notre fidélité, quitte à être presque fous, comme tous
  les grands personnages de notre histoire qui ont fait advenir l’impossible.

Écoutons cette histoire à propos de Rabbi Juda le prince, rédacteur de
la michna, et essayons d’imaginer ce qui a pu alimenter son attitude.

Ecoutons ce récit fantastique, délirant, inspirant, plein de sens en
cette soirée de Kipour.

Ce midrach est tiré du talmud babylonien baba metsia 85b. Il parle,
d’Abraham, d’Isaac et de Jacob (-2000), les trois patriarches dont la
prière pourrait modifier l’ordre du monde.
Il parle du prophète Elie (-900), dont nous attendons la venue car
il doit annoncer l’avènement d’un monde meilleur.

Il parle de Rabbi Juda hanassi (+200), le compilateur de la michna,
directeur d’une maison d’étude. Dans ce midrach, le prophète Elie est
l’un de ses étudiants. Il parle de Rabbi Hiya et de ses fils (+200),
dont la puissance de volonté est la même que celles d’Abraham d’Isaac
et de Jacob.

Eliyahou était un habitué de la maison d’étude de Rabbi. Un jour,
c’était Roch Hodech, il a pris du retard et n’est pas venu.
(Rabbi) lui a dit : « Quelle est la raison du retard du maître ? »
(Eliyahou) lui a dit : « Il a fallu que j’aide Abraham à se lever, que
je lave ses mains, qu’il finisse de prier, et que je le recouche, et
ensuite de même pour Isaac, et de même pour Jacob. »
(Rabbi 🙂 « Et qu’ils soient levés ensemble ! »
(Eliyahou 🙂 « Ils se soutiendraient dans la prière et ils feraient venir
le Messie avant son temps. »
(Rabbi) lui a dit : « Et il y a leur équivalent dans ce monde ? »
Il lui dit : « Rabbi Hiya et ses fils ».
Rabbi décréta un jeune et on fit descendre Rabbi Hiya et ses fils pour qu’ils prient.
(Rabbi Hiya) dit « il fait revenir le vent » et le vent souffla,
il dit « il fait tomber la pluie » et la pluie tomba,
quand il s’apprêta à dire « il fait revivre les morts » le monde s’émut.
On dit dans le ciel « qui a révélé les secrets dans le monde ? »
On dit « Eliyahou ».

On fit venir Eliyahou et on le bâtit de soixante bâtons de feu. Il vint
sous la forme d’un ours de feu, s’immisça entre eux et perturba leur prière.

Que de transgressions dans ce texte, qui nous montre que tout est possible !

Oui, Elie peut étudier chez Rabbi. Oui, Elie peut être le soutien des
patriarches. Oui, les patriarches peuvent continuer leurs « prières ».

Oui, leur prière pourrait bouleverser le monde et faire venir le messie
avant son temps. Oui Elie peut trahir des secrets pour permettre à Rabbi
d’agir.

Oui Rabbi peut utiliser ces secrets pour rassembler Rabbi Hiya et ses fils.
Oui Rabbi Hyia peut dans sa « prière » commander aux éléments de la nature
et bouleverser la transcendance elle-même.
Oui, tous les personnages de cette histoire sont prêts à agir, au péril
de leurs vies, pour que cessent les tourments de l’humanité.

Rabbi Juda le Prince, Elie, et Rabbi Hiya, ont tout autant soif de savoir
et d’action.

Les héros et les héroïnes de notre histoire, prêts à ces mêmes résistances
non-violentes fortes, sont innombrables.

Au-delà de leurs regrets, de leurs malheurs et de leurs doutes, ils et
elles étaient prêts à mettre leur colère et leur peine au service du
savoir et de l’action.

Que cette année 5781 nous nourrisse de savoir et d’action.

Que ces 25 heures de Kippour nous renforcent dans le savoir et l’action.

Que notre savoir, notre action, notre rassemblement soit large, suffisamment
large pour accueillir nos peines, nos colères et nos espoirs.

Nos peines, nos colères et nos espoirs, retentirons au son du chofar
dans 25 heures.
Qu’ils soient des facteurs de construction de notre savoir, de notre
action, et de notre rassemblement.

Chana Tova, Hatima Tova, tsom kal vémohil.
