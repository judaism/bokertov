.. index::
   pair: amida; 2020

.. _amida_2020:

====================================================
Boker Tov la Amida L’office de 7h30 continue 2020
====================================================

.. seealso::

 - https://rabbinchinsky.fr/2020/06/01/boker-tov-la-amida-loffice-de-7h30-continue/


.. contents::
   :depth: 3


.. _benedictions_matin:

Bénédictions du matin
=========================

barouH ata adonaï élohénou méleH haolam….

Cheassani betsalmo /  chassani bat/ben Horin   /  Cheassani Israel
/ hanoten lasseHvi bina léavHin bein yom ouvein laila   / PokéaH ivrim
/ Matir assourim  / Zokef kéfoufim   / malbich aroumim
/ Roka haarets al hamaim    / haméHin mitsadé gaver
/ cheassa lli kol tsorki   / ozer israel bigvoura
/ oter israel bétifara    / hanoten layaèf koaH


.. _chema_israel:

chéma israel
==============

.. seealso::

   - https://rabbinchinsky.fr/2020/06/01/boker-tov-la-amida-loffice-de-7h30-continue/

Chema israel adonai élohénou adonai éHad (barouH chem kévod malHouto léolam vaed)
Véahavta et adonai élohéHa béHol lévévéHa ouvéHol nafchéHa ouvéHol méodéHa
véhayou hadévarim haélé acher anoHi métsavéHa hayom al lévavéHa véchinantam
lévanéHa védibarta bam béchivtéHa béveitéHa ouvleHtéHa vadéreH ouvchoHbéHa
ouvkouméHa oukchartam léot al yadéHa véhayou létotafot bein eynéHa ouHtavtam
al mézouzot béitéHa ouvicharéHa

véhaya im chamoa tichméou el mitsvotai acher anoHi métsavé étHem hayom
léahava et adonaï élohéHem ouleovdo béHol lévavéHem ouvéHol nafchéHem
vénatati métar artséHem béito yoré oumalkoch véassafta déganéHa vétirochéHa
véitsearéHa vénatati essev béssadéHa livhèmtéHa véaHalta véssavata

Véssamtem et dévaï élé al lévavéHem véal nafchéHem oukchartem otam léot
al yédéHem véhayou létotafot bein eynéHem vélimadtem otam et beneHem
lédaber bam béchivtéHa béveitéHa ouvleHtéHa vadéreH ouvchoHbéHa ouvkouméHa
ouHtavtam al mézouzot beitéHa ouvichearéHa


.. _pensee_personelle:

pensée personnelle
====================

bilan/emerveillement/action- adonaï séfataï tiftaH oufi yaguid téhilatéHa –
BarouH ata adonaï maguen avraham vésarah, MéHayé hamétim, haèl hakadoch

- discernement – honen hadaat
- changement – harotsé bitechouva
- tourner les pages – Hanoun hamarbé lisloaH
- soulagement – goel Israël
- guérison – rofé Holé amo israel
- années – mevareH hachanim
- rassemblement – mékabets nidHé amo israel
- justice – ohev tsédaka oumichpat
- fin de la violence – chover oyeviim oumaHnia zédim
- les justes – michan oumivtaH latsadikim
- Jérusalem – boné Yérouchalaim
- La fraternité/sororité absolue – marsmiaH keren yéchoua
- Ecoute Eternel! – choméa tefila
- Retour à Sion – hamaHazir chéHinato létsion
