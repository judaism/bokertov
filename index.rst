
.. https://emojis.wiki/fr/hanoucca/

.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/bokertov/rss.xml>`_

.. _bokertov:

=========================================================================================================
🌹🌄🎶💐💗⚖ Boker Tov à toutes et tous ! בוקר טוב ​ par **Rabbin Floriane Chinsky (2020)** 🌹🌄🎶💐💗⚖
=========================================================================================================

​🔯🌄🥰🌹👬👭🌻🌈💕💙🙏💫💛💖🕊🐦🌞🎶💐 💗🍀🌺🌄
❣️💖♥️💗​😍💋😚😁🎶💐✡🔯🕎😊😁😘👌​🌻😍🙋⭐☀️
💥⚖️🌷☔👍🍎🍯👨‍👨‍👦‍👨‍👩‍👩‍👦📯🌸
🙆‍♀🙅🙆🕯

- https://rabbinchinsky.fr/
- https://judaismeenmouvement.org/les-femmes-et-hommes/floriane-chinsky/
- https://libertejuive.wordpress.com
- https://fediverse.org/DrRavFlo
- https://www.youtube.com/c/FlorianeChinsky/videos
- https://www.youtube.com/user/chirhadach
- https://fediverse.org/JudaismeM/
- https://www.youtube.com/c/FlorianeChinsky/videos
- https://judaismeenmouvement.org/agenda/judaisme-de-demain-revisiter-la-tradition/
- https://judaismeenmouvement.org/agenda/selihot-surmelin/
- https://www.youtube.com/playlist?list=PLnHlXjFx9rOQCoscQOKhH81of_tlNAjwu
- http://www.calj.net/
- https://www.hebcal.com/
- https://www.youtube.com/playlist?list=PLnHlXjFx9rOSGqqcG_o6UgLyEuaJnnb32
- https://www.youtube.com/playlist?list=PLnHlXjFx9rOSiCsY-h5WylfXoMWKN8oJQ

.. florianechinsky@gmail.com

- https://links.myjewishlearning.com/a/1161/preview/45922/1030599/1d13b1e10d8f90e69d04a8b084aada50c03c9c72?ana=InV0bV9zb3VyY2U9TUpMX01hcm9wb3N0JnV0bV9jYW1wYWlnbj1NSkxfV29yZF9vZl90aGVfRGF5JnV0bV9tZWRpdW09ZW1haWwi&message_id=IjRmOGUwYTMwLWM4MDgtMDEzYy03NTU5LTNlZTZmY2NiMTZlOEBteWpld2lzaGxlYXJuaW5nLmNvbSI=
  Morning / בּוֹקֶר
  Pronounced: BO-kehr

  The response to boker tov, "good morning," is boker or,
  which means "morning of light."

.. toctree::
   :maxdepth: 4

   2020/2020
   fetes/fetes
