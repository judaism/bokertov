.. index::
   ! Hanoucca

.. _hanoucca:

============================================
Hanoucca 🕎
============================================

.. seealso::

   - https://emojis.wiki/fr/hanoucca/
   - https://akademimg.akadem.org/Medias/Documents/Adam-hanouca.pdf

.. contents::
   :depth: 3


Joyeux Hanoucca! 🕎🔯

Définition
============

Vous trouverez ici la liste des émojis de Hanoucca que vous pouvez utiliser
pour célébrer cette fête et la partager avec vos meilleurs amis.

Hanoucca est une célèbre fête juive qui a lieu annuellement.

Elle signifie le triomphe de la foi et du courage sur la puissance militaire.

A l´origine c´est l´histoire d´un petit groupe d’Israéliens qui ont
commencé à défendre leurs droits d’être juifs.

C’est une fête importante pour le peuple juif qui dure 8 jours d’affilées.

La tradition veut que, durant cette période, on allume huit bougies 🕯.

On mange des crêpes de pommes de terre 🥞 et les enfants reçoivent de
l’argent 💰 comme cadeau 🎁.

Citations
===========

L’obscurité du monde entier ne peut pas éteindre la flamme d’une bougie.
— Robert Altinger

Le miracle, bien sûr, n’est pas que l’huile de la lumière sacrée — dans
une petite cruche — ait tenu aussi longtemps qu’on le dit; mais que le
courage des Maccabées tient jusqu’à ce jour: que cela puisse nourrir
mon esprit vacillant.

— Charles Reznikoff

Les faits
===============

« Dévouement » est la signification exacte du mot Hanoucca.
La plus grande Menorah du monde se trouve sur la place « Grand Army », 🗽 New York.
C’est 👨 💼 Harry Truman qui a célébré Hanoucca pour la première fois à
la Maison Blanche.


LES FONDAMENTAUX du Judaïsme--HANOUKKA--Qu'est-ce que les lumières ?
========================================================================

.. seealso::

   - https://www.youtube.com/watch?v=8WveURIUloU

Hanoukka a la particularité d’articuler la mémoire d’événements militaires,
nationalistes, issus de la dure réalité géopolitique moyen-orientale,
avec les plus hautes aspirations spirituelles : la lumière, symbole d’intelligence,
de signification, de transmission…

Ses prescriptions nous élèvent jusqu’à la dimension de la « sur-nature ».

Par ses huit jours elle est la Fête la plus longue, elle tutoie l’infini,
ce huit renversé dont le mathématicien Wallis fit le symbole.

Renversement du regard, également : alors même que la lumière, est le
médium, invisible, qui nous rend tout visible, il nous est demandé en
ces soirées de joie de ne point les « utiliser », et de les regarder
pour elles-mêmes.

Cette douce friction du « naturel » et de ce qui l’excède, ce glissement
imperceptible, ce léger « au-delà », serait-ce là une simple définition
du miracle ?

De la glorieuse saga des Macchabées – par la suite atténuée par les rabbins,
en passant par la ré-inauguration du Temple (« ‘hanoukkat ha-Baïth »),
nous évoquerons les aspects historiques, rituels et liturgiques de
la « Fête des lumières », et nous nous laisserons glisser, à la lueur
de leur pointe dansante, à une réévaluation de la question kantienne :
pour la tradition juive, qu’est-ce que les lumières ?
